package com.example.user.aes;

import android.app.Activity;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;


public class MainActivity extends ActionBarActivity {
    private static final String LOGTAG = "MainActivity";
    private final int REQUEST_CODE_PICK_DIR = 1;
    private final int REQUEST_CODE_PICK_FILE = 2;
    public static String sourcePath = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnAes = (Button) findViewById(R.id.btnAes);
        Button btnDecode = (Button) findViewById(R.id.btnDecode);
        Button btnChoseFileEncode = (Button) findViewById(R.id.btnChoseFileEncode);
        //Button btnChoseFileDecode = (Button)findViewById(R.id.btnChoseFileDecode);

        final EditText edDestinationFile = (EditText) findViewById(R.id.edDestenationFileStego);
        final EditText edInputMessage = (EditText) findViewById(R.id.edInputMessage);
        final EditText edOutputFile = (EditText) findViewById(R.id.edOutputFile);
        final TextView tvOutputMessage = (TextView) findViewById(R.id.tvOutputMessage);
        final EditText edSourceFile = (EditText) findViewById(R.id.edSourceFileStego);
        final Activity activityForButton = this;

        View.OnClickListener search = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent fileExploreIntent = new Intent(
                        FileBrowserActivity.INTENT_ACTION_SELECT_FILE,
                        null,
                        activityForButton,
                        FileBrowserActivity.class
                );

                startActivityForResult(
                        fileExploreIntent,
                        REQUEST_CODE_PICK_FILE
                );
            }
        };
        btnChoseFileEncode.setOnClickListener(search);

        View.OnClickListener decodeFile = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nameFile = String.valueOf(edOutputFile.getText());
                String nameOutFile = "decrypted_" + nameFile;
                try {
                    decryptOurMessage(nameFile, nameOutFile);
                    tvOutputMessage.setText("File was decode");
                } catch (IOException e) {
                    e.printStackTrace();
                    tvOutputMessage.setText("Bad output file");
                }
            }
        };
        btnDecode.setOnClickListener(decodeFile);


    View.OnClickListener doAes = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nameFile;
                if (sourcePath != null) {
                    nameFile = sourcePath;
                    edSourceFile.setText(nameFile);
                }
                nameFile = String.valueOf(edSourceFile.getText());
                if (String.valueOf(edInputMessage.getText()).length()>16) {
                    Toast.makeText(getBaseContext(), "bad input key", Toast.LENGTH_LONG).show();
                    return;
                }
                String message = String.valueOf(edInputMessage.getText());
                String nameOutFile = String.valueOf(edDestinationFile.getText());

                // строка которую надо вставить в наш файл
                // зашифровываем
                try {
                    encryptOurMessage(nameFile, message, nameOutFile);
                } catch (IOException e) {
                    e.printStackTrace();
                    tvOutputMessage.setText("Bad input file");
                }
            }
        };
        btnAes.setOnClickListener(doAes);
    }

    private void decryptOurMessage(String nameFile, String nameOutFile) throws IOException  {
        File sdPath = Environment.getExternalStorageDirectory();
        if (sourcePath!=null) {
            int lenOfNameFile=1;
            while (sourcePath.charAt(sourcePath.length()-lenOfNameFile-1)!='/')
            {
                lenOfNameFile++;
            }
            sdPath = new File (sourcePath.substring(0,sourcePath.length()-lenOfNameFile));
        }
        //sdPath = new File(sdPath.getAbsolutePath() + "/" + DIR_SD);
        // формируем объект File, который содержит путь к файлу
        File sdFile = new File(sdPath, nameFile);
        RandomAccessFile fileToDecode = new RandomAccessFile(sdFile, "rw");
        sdFile = new File(sdPath,nameOutFile);
        RandomAccessFile fileOut = new RandomAccessFile(sdFile, "rw");

        while (true) {
            boolean needExit = readBlock(fileToDecode);
            SupportFunctions act = new SupportFunctions();
            act.Decryption();
            writeBlock(fileOut);
            if(needExit) {
                fileOut.close();
                fileToDecode.close();
                return;
            }
        }

    };

    private void encryptOurMessage(String nameFile, String message, String nameOutFile)        throws IOException {
        if (!Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED)) {
            Log.e(LOCATION_SERVICE, "bad card");
            return;
        }
        File sdPath;
        int lenOfNameFile = 1;
        while (nameFile.charAt(nameFile.length() - lenOfNameFile - 1) != '/') {
            lenOfNameFile++;
        }
        sdPath = new File(nameFile.substring(0, nameFile.length() - lenOfNameFile));
        nameFile = nameFile.substring(nameFile.length() - lenOfNameFile, nameFile.length());

        File sdInputFile = new File(sdPath, nameFile);
        File sdOutFile = new File(sdPath, nameOutFile);

        RandomAccessFile file = new RandomAccessFile(sdInputFile, "rw");
        RandomAccessFile outFile = new RandomAccessFile(sdOutFile, "rw");
        int i;
        for (i = 0; i < message.length(); i++) {
            SupportFunctions.Key[i] = message.toCharArray()[i];
        }
        for (; i < 16; i++)
            SupportFunctions.Key[i] = 0x01;

        while (true) {
            boolean needExit = readBlock(file);
            SupportFunctions act = new SupportFunctions();
            act.Encryption();
            writeBlock(outFile);
            if(needExit) {
                outFile.close();
                file.close();
                return;
            }
        }
    }

    private int writeBlock(RandomAccessFile outFile) {
        for (int i = 0; i < 16; i++) {
            try {
                outFile.writeByte(SupportFunctions.out[i]);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return 0;
    }
    private boolean readBlock(RandomAccessFile file){
        int currentByte = 0;
        boolean endFile = false;
        char [] inBlockOf16Byte = new char[16];
        for(int i = 0; i<16;i++) {
            try {
                currentByte = file.read();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (currentByte == -1) {
                for (; i < 16; i++) {
                   inBlockOf16Byte[i] = 0x00;
                   endFile = true;
                }
                break;
            }
            inBlockOf16Byte[i] = (char) ( currentByte%256);//на всякий случай
        }
        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 4; j++)
                SupportFunctions.state[i][j] = inBlockOf16Byte[i + j*4];
        }
        return endFile;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_PICK_DIR) {
            if(resultCode == RESULT_OK) {
                String newDir = data.getStringExtra(//получаем строку с папкой
                        FileBrowserActivity.returnDirectoryParameter);

                Toast.makeText(
                        this,
                        "Received DIRECTORY path from file browser:\n" + newDir,
                        Toast.LENGTH_LONG).show();

            } else {//if(resultCode == this.RESULT_OK) {
                Toast.makeText(
                        this,
                        "Received NO result from file browser",
                        Toast.LENGTH_LONG).show();
            }//END } else {//if(resultCode == this.RESULT_OK) {
        }//if (requestCode == REQUEST_CODE_PICK_DIR) {

        if (requestCode == REQUEST_CODE_PICK_FILE) {
            if(resultCode == RESULT_OK) {
                String newFile = data.getStringExtra(
                        FileBrowserActivity.returnFileParameter);
                sourcePath = newFile;
                EditText ed = (EditText) findViewById(R.id.edSourceFileStego);
                ed.setText(newFile);
                //edSourceFile.setText(newFile);
                Toast.makeText(
                        this,
                        "Received FILE path from file browser:\n"+newFile,
                        Toast.LENGTH_LONG).show();

            } else {//if(resultCode == this.RESULT_OK) {
                Toast.makeText(
                        this,
                        "Received NO result from file browser",
                        Toast.LENGTH_LONG).show();
            }//END } else {//if(resultCode == this.RESULT_OK) {
        }//if (requestCode == REQUEST_CODE_PICK_FILE) {


        super.onActivityResult(requestCode, resultCode, data);

    }
}
