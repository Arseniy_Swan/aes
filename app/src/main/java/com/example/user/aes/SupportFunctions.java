package com.example.user.aes;

//import com.example.user.aes.MainActivity;
/**
 * Created by user on 04.06.2015.
 */
public class SupportFunctions {
    private static final char TOBYTE = 256;
    public static int SC = 4; // number of colomns of state (for AES SC = 4) Nb
    // state is intermediate resulf of crypting (matrix 4*SC)
    public static int R = 10; // number of rounds for crypting Nr
    public static int KL = 4; // 4 * 32 = 128 Nk
    public static char[] in = new char[16], out = new char[16];
    public static char[][] state = new char[4][SC];
    public static char[] Key = new char[16];
    boolean llog;

    public static char[][] RoundKey = new char[4][KL * (R + 1)];

  /*  char first_key[] = {0x11, 0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20}; //default key;
    char block[] = {0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f, 0x10}; //default text
    int c = 0, lc = 0;
    String key="";*/

/*
    void initKey(char[] first_key) {
        for (int i = 0; i < 16; i++)
            key += first_key[i % first_key.length];
    }
*/



    public char Sub_Matrix(char val) {
        int Sm[] =//256
                {
                        //0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
                        0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76, //0
                        0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0, //1
                        0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15, //2
                        0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75, //3
                        0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84, //4
                        0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf, //5
                        0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8, //6
                        0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2, //7
                        0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73, //8
                        0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb, //9
                        0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79, //A
                        0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08, //B
                        0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a, //C
                        0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e, //D
                        0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf, //E
                        0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16  //F
                };

        return (char) Sm[val];

    }

    public char R_Sub_Matrix(char val) {
        int RSm[] =//256
                {
                        //0     1    2      3     4    5     6     7      8    9     A      B    C     D     E     F
                        0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb, //0
                        0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb, //1
                        0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e, //2
                        0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25, //3
                        0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92, //4
                        0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84, //5
                        0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06, //6
                        0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b, //7
                        0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73, //8
                        0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e, //9
                        0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b, //A
                        0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4, //B
                        0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f, //C
                        0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef, //D
                        0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61, //E
                        0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d  //F
                };

        return (char) RSm[val];
    }

    void XorroundKey(int round) {
        for (int j = 0; j < KL; j++) {
            for (int i = 0; i < 4; i++) {
                state[i][j] = (char) (state[i][j] ^ RoundKey[i][round * SC + j]);
            }
        }
    }

    void ShiftRows() {
        char temp;

        // Rotate first row 1 columns to left
        temp = state[1][0];
        state[1][0] = state[1][1];
        state[1][1] = state[1][2];
        state[1][2] = state[1][3];
        state[1][3] = temp;

        // Rotate second row 2 columns to left
        temp = state[2][0];
        state[2][0] = state[2][2];
        state[2][2] = temp;

        temp = state[2][1];
        state[2][1] = state[2][3];
        state[2][3] = temp;

        // Rotate third row 3 columns to left
        temp = state[3][0];
        state[3][0] = state[3][3];
        state[3][3] = state[3][2];
        state[3][2] = state[3][1];
        state[3][1] = temp;
    }

    void InvShiftRows() {
        char temp;

        // Rotate first row 1 columns to right
        temp = state[1][3];
        state[1][3] = state[1][2];
        state[1][2] = state[1][1];
        state[1][1] = state[1][0];
        state[1][0] = temp;

        // Rotate second row 2 columns to right
        temp = state[2][0];
        state[2][0] = state[2][2];
        state[2][2] = temp;

        temp = state[2][1];
        state[2][1] = state[2][3];
        state[2][3] = temp;

        // Rotate third row 3 columns to right
        temp = state[3][0];
        state[3][0] = state[3][1];
        state[3][1] = state[3][2];
        state[3][2] = state[3][3];
        state[3][3] = temp;
    }

    void SubBytes() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                state[i][j] = Sub_Matrix(state[i][j]);
            }
        }
    }

    void InvSubBytes() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                state[i][j] = R_Sub_Matrix(state[i][j]);
            }
        }
    }

    void MixColumns() {
        char s1, s2, s3, s4;
        for (int j = 0; j < 4; j++) {
            s1 = (char) ((mul_by_two(state[0][j]) ^ mul_by_three(state[1][j]) ^ state[2][j] ^ state[3][j]) % TOBYTE);
            s2 = (char) ((state[0][j] ^ mul_by_two(state[1][j]) ^ mul_by_three(state[2][j]) ^ state[3][j]) % TOBYTE);
            s3 = (char) ((state[0][j] ^ state[1][j] ^ mul_by_two(state[2][j]) ^ mul_by_three(state[3][j])) % TOBYTE);
            s4 = (char) ((mul_by_three(state[0][j]) ^ state[1][j] ^ state[2][j] ^ mul_by_two(state[3][j])) % TOBYTE);
            state[0][j] = s1;
            state[1][j] = s2;
            state[2][j] = s3;
            state[3][j] = s4;
        }
    }

   /* private void Print_State() {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++)
                System.out.print(state[i][j]);
        }
        System.out.println();
    }*/

    void InvMixColumns() {
        char s1, s2, s3, s4;
        for (int j = 0; j < 4; j++) {//хотя так никогда и не будет, что при операции ксор число стало больше чем TOBYTE(256), но перестрахуемся
            s1 = (char) ((mul_by_E(state[0][j]) ^ mul_by_B(state[1][j]) ^ mul_by_D(state[2][j]) ^ mul_by_nine(state[3][j])) % TOBYTE);
            s2 = (char) ((mul_by_nine(state[0][j]) ^ mul_by_E(state[1][j]) ^ mul_by_B(state[2][j]) ^ mul_by_D(state[3][j])) % TOBYTE);
            s3 = (char) ((mul_by_D(state[0][j]) ^ mul_by_nine(state[1][j]) ^ mul_by_E(state[2][j]) ^ mul_by_B(state[3][j])) % TOBYTE);
            s4 = (char) ((mul_by_B(state[0][j]) ^ mul_by_D(state[1][j]) ^ mul_by_nine(state[2][j]) ^ mul_by_E(state[3][j])) % TOBYTE);

/*
            s1 = (char) ((multiply(state[0][j],'E') ^ multiply(state[1][j],'B') ^ multiply(state[2][j],'D') ^ multiply(state[3][j],'9')) % TOBYTE);
            s2 = (char) ((multiply(state[0][j],'9') ^ multiply(state[1][j],'E') ^ multiply(state[2][j],'B') ^ multiply(state[3][j],'D')) % TOBYTE);
            s3 = (char) ((multiply(state[0][j],'D') ^ multiply(state[1][j],'9') ^ multiply(state[2][j],'E') ^ multiply(state[3][j],'B')) % TOBYTE);
            s4 = (char) ((multiply(state[0][j],'B') ^ multiply(state[1][j],'D') ^ multiply(state[2][j],'9') ^ multiply(state[3][j],'E')) % TOBYTE);
*/
            state[0][j] = s1;
            state[1][j] = s2;
            state[2][j] = s3;
            state[3][j] = s4;
        }
    }

   /* char multiply(char val, char mul){
        switch (mul) {
            case '2':
                if (val < 0x80)
                    val = (char) (val << 1);
                else
                    val = (char) ((val << 1) ^ 0x1b);
                return (char) (val % 0x100);
            case '3':
                return (char) (multiply(val, '2') ^ val);
            case '9':
                return (char) ((multiply(multiply(multiply(val,'2'),'2'),'2')) ^ val);
            case 'B':
                return (char) (multiply(val,'9') ^ multiply(val,'2') ^ val);
            case 'D':
                return (char) (multiply(val,'9') ^ multiply(multiply(val,'2'),'2') ^val);
            case 'E':
                return (char) (multiply(val,'9') ^ multiply(multiply(val,'2'),'2') ^ multiply(val,'2'));
        }

        return val;
    }*/
    char mul_by_two(char val) {
        if (val < 0x80)
            val = (char) (val << 1);
        else
            val = (char) ((val << 1) ^ 0x1b);
        return (char) (val % 0x100);
    }

    char mul_by_three(char val) {
        return (char) (mul_by_two(val) ^ val);
    }

    char mul_by_nine(char val) {
        return (char) (mul_by_two(mul_by_two(mul_by_two(val))) ^ val);
    }

    char mul_by_B(char val) {
        return (char) (mul_by_two(mul_by_two(mul_by_two(val))) ^ mul_by_two(val) ^ val);
    }

    char mul_by_D(char val) {
        return (char) (mul_by_two(mul_by_two(mul_by_two(val))) ^ mul_by_two(mul_by_two(val)) ^ val);
    }

    char mul_by_E(char val) {
        return (char) (mul_by_two(mul_by_two(mul_by_two(val))) ^ mul_by_two(mul_by_two(val)) ^ mul_by_two(val));
    }
    void KeyGeneration()
    {
        int[][] rcon = {
                {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36},
                {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00},
                {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00}
        };
        char temp[] = new char[4], k;

        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < KL; j++)
            {
                RoundKey[i][j] = Key[i + 4*j];
            }
        }
        for(int i = KL; i < KL*(R+1); i++)
        {
            for (int j = 0; j < 4; j++)
            {
                temp[j] = RoundKey[j][i-1];
            }

            if (i % KL == 0)
            {
                k = temp[0];
                temp[0] = temp[1];
                temp[1] = temp[2];
                temp[2] = temp[3];
                temp[3] = k;

                for (int j = 0; j < 4; j++)
                {
                    temp[j]= Sub_Matrix(temp[j]);
                }
                for (int j = 0; j < 4; j++)
                    temp[j] = (char) ((RoundKey[j][i-KL]) ^ (temp[j]) ^ (rcon[j][i/KL-1]));
            }
            else
            {
                for (int j = 0; j < 4; j++)
                    temp[j] = (char) (RoundKey[j][i-KL] ^ RoundKey[j][i-1]); // temp[j] = temp[j] ^ RoundKey[j][i-KL];
            }
            for (int j = 0; j < 4; j++)
                RoundKey[j][i] = temp[j];
        }
    }

    public void Encryption() {
        KeyGeneration();
        XorroundKey(0);

        for (int round = 1; round < R; round++) {
            SubBytes();
            ShiftRows();
            MixColumns();
            XorroundKey(round);
        }

        SubBytes();

        ShiftRows();
        XorroundKey(R);

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                out[i * 4 + j] = state[j][i];
            }
        }
    }
    void Decryption()
    {
        XorroundKey(R);
        for(int round = R-1; round > 0; round--)
        {

            InvShiftRows();
            InvSubBytes();
            XorroundKey(round);
            InvMixColumns();
        }

        InvShiftRows();
        InvSubBytes();
        XorroundKey(0);

        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                out[i*4+j] = state[j][i];
            }
        }
    }
}